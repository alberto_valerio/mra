package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	public static final String DEFAULT_STATUS = "(0,0,N)";
	private int xDimension;
	private int yDimension;
	private List<String> obstacles;

	private int[][] planet;
	private String roverStatus = DEFAULT_STATUS;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		int counterErr = 0;
		for (int i = 0; i < planetObstacles.size(); i++) {
			String obstacle = planetObstacles.get(i);
			String[] coords = obstacle.substring(1, obstacle.length() - 1).split(",");
			if(Integer.parseInt(coords[0]) < 0 || Integer.parseInt(coords[0]) >= planetX || Integer.parseInt(coords[1]) < 0 || Integer.parseInt(coords[1]) >= planetY) {
				counterErr++;
			}
		}
		if(counterErr > 0) {
			throw new MarsRoverException("planetObstacle contain out of range values!");
		} else {
			this.xDimension = planetX;
			this.yDimension = planetY;
			this.obstacles = planetObstacles;
			this.planet = new int[planetX][planetY];

			for (int i = 0; i < obstacles.size(); i++) {
				String obstacle = obstacles.get(i);
				String[] coords = obstacle.substring(1, obstacle.length() - 1).split(",");
				planet[Integer.parseInt(coords[1])][Integer.parseInt(coords[0])] = 'X';
			}
		}
			
	}

	
	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if (x < 0 || x >= xDimension || y < 0 || y >= yDimension) {
			throw new MarsRoverException("Out of range index!");
		} else {
			return planet[y][x] == 'X';
		}
	}

	
	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if (commandString.equals("")) {
			
			roverStatus = DEFAULT_STATUS;
			
		} else {
			
			for (int i = 0; i < commandString.length(); i++){
			    char command = commandString.charAt(i);        
			    switch (command) {
					case 'r':
						turnRover("r");
						break;
					case 'l':
						turnRover("l");
						break;
					case 'f':
						moveRoverForward();
						break;
					case 'b':
						moveRoverBackword();
						break;
					default:
						throw new MarsRoverException("commandString contain not valid instruction!");
				}
			}
			
		}
		return roverStatus;
	}
	
	
	public void setRoverStatus(String command) {
		roverStatus = command;
	}
	
	private void turnRover(String side) {
		String[] roverInfo =  roverStatus.substring(1, roverStatus.length() - 1).split(",");
		if (side.equals("r")) {
			roverStatus = "("+roverInfo[0]+","+roverInfo[1]+","+"E"+")";
		}
		if (side.equals("l")) {
			roverStatus = "("+roverInfo[0]+","+roverInfo[1]+","+"W"+")";
		}
	}
	
	private void moveRoverForward() throws MarsRoverException {
		String[] roverInfo =  roverStatus.substring(1, roverStatus.length() - 1).split(",");
		int newPosition;
		
		if (roverInfo[2].equals("N")) {
			if (Integer.parseInt(roverInfo[1]) == yDimension - 1) {
				newPosition = 0;
			} else {
				newPosition = Integer.parseInt(roverInfo[1]) + 1;
			}
			roverStatus = "("+roverInfo[0]+","+newPosition+","+roverInfo[2]+")"+obstacleEncountered(Integer.parseInt(roverInfo[0]), newPosition);			
		}
		if (roverInfo[2].equals("S")) {
			if (Integer.parseInt(roverInfo[1]) == yDimension - 1) {
				newPosition = 0;
			} else {
				newPosition = Integer.parseInt(roverInfo[1]) - 1;
			}
			roverStatus = "("+roverInfo[0]+","+newPosition+","+roverInfo[2]+")"+obstacleEncountered(Integer.parseInt(roverInfo[0]), newPosition);
		}
		if (roverInfo[2].equals("E")) {
			if (Integer.parseInt(roverInfo[0]) == yDimension - 1) {
				newPosition = 0;
			} else {
				newPosition = Integer.parseInt(roverInfo[0]) + 1;
			}
			roverStatus = "("+newPosition+","+roverInfo[1]+","+roverInfo[2]+")"+obstacleEncountered(newPosition,Integer.parseInt(roverInfo[1]));
		}
		if (roverInfo[2].equals("W")) {
			if (Integer.parseInt(roverInfo[0]) == yDimension - 1) {
				newPosition = 0;
			} else {
				newPosition = Integer.parseInt(roverInfo[0]) - 1;
			}
			roverStatus = "("+newPosition+","+roverInfo[1]+","+roverInfo[2]+")"+obstacleEncountered(newPosition,Integer.parseInt(roverInfo[1]));
		}
	}

	private void moveRoverBackword() throws MarsRoverException {
		String[] roverInfo =  roverStatus.substring(1, roverStatus.length() - 1).split(",");
		int newPosition;
		
		if (roverInfo[2].equals("N")) {
			if (Integer.parseInt(roverInfo[1]) == 0) {
				newPosition = yDimension - 1;
			} else {
				newPosition = Integer.parseInt(roverInfo[1]) - 1;
			}
			roverStatus = "("+roverInfo[0]+","+newPosition+","+roverInfo[2]+")"+obstacleEncountered(Integer.parseInt(roverInfo[0]),newPosition);			
		}
		if (roverInfo[2].equals("S")) {
			if (Integer.parseInt(roverInfo[1]) == 0) {
				newPosition = yDimension - 1;
			} else {
				newPosition = Integer.parseInt(roverInfo[1]) + 1;
			}
			roverStatus = "("+roverInfo[0]+","+newPosition+","+roverInfo[2]+")"+obstacleEncountered(Integer.parseInt(roverInfo[0]),newPosition);
		}
		if (roverInfo[2].equals("E")) {
			if (Integer.parseInt(roverInfo[0]) == 0) {
				newPosition = xDimension - 1;
			} else {
				newPosition = Integer.parseInt(roverInfo[0]) - 1;
			}
			roverStatus = "("+newPosition+","+roverInfo[1]+","+roverInfo[2]+")"+obstacleEncountered(newPosition, Integer.parseInt(roverInfo[1]));
		}
		if (roverInfo[2].equals("W")) {
			if (Integer.parseInt(roverInfo[0]) == 0) {
				newPosition = xDimension - 1;
			} else {
				newPosition = Integer.parseInt(roverInfo[0]) + 1;
			}
			roverStatus = "("+newPosition+","+roverInfo[1]+","+roverInfo[2]+")"+obstacleEncountered(newPosition, Integer.parseInt(roverInfo[1]));
		}
	}
	
	
	private String obstacleEncountered(int x, int y) throws MarsRoverException {
		if (planetContainsObstacleAt(x,y)) {
			return "("+x+","+y+")";
		}
		return "";
	}

	

}

package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	/** USER STORY #1 **/
	@Test
	public void testPlanetCreationWithObstacleAt() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(rover.planetContainsObstacleAt(4, 7));	
	}
	@Test (expected = MarsRoverException.class)
	public void testPlanetCreationWithObstacleAtOutOfRangeValue() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,10)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		rover.planetContainsObstacleAt(4, 10);	
	}
	@Test (expected = MarsRoverException.class)
	public void testPlanetCreationWithOutOfRangeValue() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,10)");
		planetObstacles.add("(2,3)");
		new MarsRover(10, 10, planetObstacles);
	}
	/** END USER STORY #1 **/
	
	
	/** USER STORY #2 **/
	@Test
	public void testExecuteEmptyCommand() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	/** END USER STORY #2 **/

	
	/** USER STORY #3 **/
	@Test
	public void testExecuteTurnRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	@Test
	public void testExecuteTurnLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	/** END USER STORY #3 **/
	
	
	/** USER STORY #4 **/
	@Test
	public void testMoveForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		rover.setRoverStatus("(7,6,N)");

		assertEquals("(7,7,N)", rover.executeCommand("f"));
	}
	/** END USER STORY #4 **/

	
	/** USER STORY #5 **/
	@Test
	public void testMoveBackword() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		rover.setRoverStatus("(5,8,E)");

		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	/** END USER STORY #5 **/

	
	/** USER STORY #6 **/
	@Test
	public void testExecuteCombinedCommands() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		rover.setRoverStatus("(0,0,N)");

		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	/** END USER STORY #6 **/

	
	/** USER STORY #7 **/
	@Test
	public void testMoveBackwordBeyondBottomEdge() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		rover.setRoverStatus("(0,0,N)");

		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	@Test
	public void testMoveBackwordBeyondLeftEdge() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		rover.setRoverStatus("(0,0,E)");

		assertEquals("(9,0,E)", rover.executeCommand("b"));
	}
	@Test
	public void testMoveForwardBeyondTopEdge() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		rover.setRoverStatus("(0,9,N)");

		assertEquals("(0,0,N)", rover.executeCommand("f"));
	}
	@Test
	public void testMoveForwardBeyondRightEdge() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		rover.setRoverStatus("(9,0,E)");

		assertEquals("(0,0,E)", rover.executeCommand("f"));
	}
	/** END USER STORY #7 **/
	
	
	/** USER STORY #8 **/
	/// NOT WORKING - TIME EXIPRED!
//	@Test
//	public void testEncounterSingleObstacle() throws MarsRoverException {
//		List<String> planetObstacles = new ArrayList<>();
//		planetObstacles.add("(2,2)");
//		MarsRover rover = new MarsRover(10, 10, planetObstacles);
//
//		rover.setRoverStatus("(0,0,N)");
//
//		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
//	}
	/** END USER STORY #8 **/

	
}
